//
//  CGPoint.swift
//  GameEngine
//
//  Created by Melvin Tan Jun Keong on 7/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import UIKit

extension CGPoint {
    /// Obtain the distance from itself to a given point
    func distanceTo(point: CGPoint) -> CGFloat {
        return CGFloat(hypotf(Float(x - point.x), Float(y - point.y)))
    }
    
    /// Obtain the direction from itself to a given point
    func directionTo(point: CGPoint) -> CGPoint {
        return CGPointMake(point.x - x, point.y - y)
    }
    
    /// Obtain the standardized direction from itself to a given point
    func standardizedDirectionTo(point: CGPoint) -> CGPoint {
        let distance = distanceTo(point)
        return CGPointMake((point.x - x)/distance, (point.y - y)/distance)
    }
    
    /// Multiply the vector CGPoint by a given scalar amount CGFloat
    func scale(amount: CGFloat) -> CGPoint {
        return CGPointMake(x * amount, y * amount)
    }
    
    /// Reflect along the Y-axis
    func reflectX() -> CGPoint {
        return CGPointMake(-x, y)
    }
    
    /// Reflect along the X-axis
    func reflectY() -> CGPoint {
        return CGPointMake(x, -y)
    }
    
    /// Convert location CGPoint into bubble coordinates in the bubbles area
    func toBubblesAreaCoordinates() -> Pair<Int, Int> {
        let scale = Constants.bubbleHeightScaled
        let row = Int(y / scale)
        let column = (row % 2 == 0) ? Int(x / Constants.bubbleDiameter) :
                                      Int((x - Constants.bubbleRadius) / Constants.bubbleDiameter)
        return Pair(row, column)
    }
}