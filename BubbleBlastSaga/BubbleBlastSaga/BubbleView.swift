//
//  BubbleView.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 3/2/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

import UIKit

class BubbleView: UIView {
    private var bubbleType: Constants.BubbleType?
    
    init(frame: CGRect, playMode: Bool) {
        bubbleType = nil
        super.init(frame: frame)
        self.backgroundColor = playMode ? UIColor.whiteColor().colorWithAlphaComponent(Constants.transparent) :
            UIColor.whiteColor().colorWithAlphaComponent(Constants.translucent)
        self.layer.cornerRadius = Constants.bubbleRadius
        self.layer.borderWidth = playMode ? Constants.doNotShowBorder : Constants.showBorder
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getBubbleType() -> Constants.BubbleType? {
        return bubbleType
    }
    
    /// Checks if the bubble view has a bubble type view or is empty.
    func hasBubbleType() -> Bool {
        return bubbleType != nil
    }
    
    /// Adds a given bubble type image subview to the bubble view
    func addBubbleType(type: Constants.BubbleType) {
        removeBubbleType()
        let bubbleImageFilename = Constants.bubbleFilenames[type]!
        let bubbleImage = UIImage(named: bubbleImageFilename)
        let bubble = UIImageView(frame: self.bounds)
        bubble.image = bubbleImage
        self.addSubview(bubble)
        bubbleType = type
    }
    
    /// Removes a bubble type view from the bubble view
    func removeBubbleType() {
        if hasBubbleType() {
            self.subviews.forEach { $0.removeFromSuperview() }
            bubbleType = nil
        }
    }
    
    /// Removes a bubble type view from the bubble view by fading out
    func removeBubbleTypeByBursting() {
        if hasBubbleType() && getBubbleType()! != Constants.BubbleType.Indestructible {
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            UIView.animateWithDuration(Constants.animationDuration,
                delay: Constants.noAnimationDelay,
                options: .CurveEaseOut,
                animations: {
                    self.subviews.first!.alpha = Constants.transparent
                },
                completion: { finished in
                    self.subviews.forEach { $0.removeFromSuperview() }
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
            )
            self.bubbleType = nil
        }
    }
    
    /// Removes a bubble type view from the bubble view by falling off the screen
    func removeBubbleTypeByDropping() {
        if hasBubbleType() {
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            UIView.animateWithDuration(Constants.animationDuration,
                delay: Constants.noAnimationDelay,
                options: .CurveEaseIn,
                animations: {
                    self.subviews.first!.center.y += Constants.totalScreenHeight
                },
                completion: { finished in
                    self.subviews.forEach { $0.removeFromSuperview() }
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
            )
            self.bubbleType = nil
        }
    }
    
    /// Removes a bubble type view from the bubble view by turning the view into a lightning bubble while fading out.
    func removeBubbleTypeByLightning() {
        if hasBubbleType() && getBubbleType()! != Constants.BubbleType.Indestructible {
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            UIView.animateWithDuration(Constants.animationDuration,
                delay: Constants.noAnimationDelay,
                options: .CurveEaseIn,
                animations: {
                    self.addBubbleType(Constants.BubbleType.Lightning)
                    self.subviews.first!.alpha = Constants.transparent
                },
                completion: { finished in
                    self.subviews.forEach { $0.removeFromSuperview() }
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
            )
            self.bubbleType = nil
        }
    }
    
    /// Removes a bubble type view from the bubble view by turning the view into a bomb bubble while fading out.
    func removeBubbleTypeByBomb() {
        if hasBubbleType() && getBubbleType()! != Constants.BubbleType.Indestructible {
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            UIView.animateWithDuration(Constants.animationDuration,
                delay: Constants.noAnimationDelay,
                options: .CurveEaseIn,
                animations: {
                    self.addBubbleType(Constants.BubbleType.Bomb)
                    self.subviews.first!.alpha = Constants.transparent
                },
                completion: { finished in
                    self.subviews.forEach { $0.removeFromSuperview() }
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
            )
            self.bubbleType = nil
        }
    }
    
    /// Removes a bubble type view from the bubble view by turning the view into a star bubble while fading out.
    func removeBubbleTypeByStar() {
        if hasBubbleType() && getBubbleType()! != Constants.BubbleType.Indestructible {
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            UIView.animateWithDuration(Constants.animationDuration,
                delay: Constants.noAnimationDelay,
                options: .CurveEaseOut,
                animations: {
                    self.addBubbleType(Constants.BubbleType.Star)
                    self.subviews.first!.alpha = Constants.transparent
                },
                completion: { finished in
                    self.subviews.forEach { $0.removeFromSuperview() }
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
            )
            self.bubbleType = nil
        }
    }
}