//
//  Constants.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 30/1/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

import UIKit

struct Constants {
    enum BubbleType {
        case Red, Orange, Green, Blue, Indestructible, Lightning, Bomb, Star
    }
    
    static let redBubbleFilename            = "bubble-red.png"
    static let orangeBubbleFilename         = "bubble-orange.png"
    static let greenBubbleFilename          = "bubble-green.png"
    static let blueBubbleFilename           = "bubble-blue.png"
    static let indestructibleBubbleFilename = "bubble-indestructible.png"
    static let lightningBubbleFilename      = "bubble-lightning.png"
    static let bombBubbleFilename           = "bubble-bomb.png"
    static let starBubbleFilename           = "bubble-star.png"
    static let cannonFramesFilename         = "cannon.png"
    static let baseFilename                 = "cannon-base.png"
    static let vineFilename                 = "foot-vine.png"
    static let backgroundFilename           = "background.png"
    static let background2Filename          = "background-2.png"
    static let allSoloPlayerLevelFilenames  = ["Basic Bubbles",
                                               "Drop Like Grapes",
                                               "Lightning Bubbles",
                                               "Bomb Bubbles",
                                               "Star Bubbles",
                                               "Chain Reaction"]
    static let allMultiPlayerLevelFilenames = ["Multiplayer Random Stage 1",
                                               "Multiplayer Random Stage 2",
                                               "Multiplayer Random Stage 3",
                                               "Multiplayer Random Stage 4",
                                               "Multiplayer Random Stage 5",
                                               "Multiplayer Random Stage 6",
                                               "Multiplayer Random Stage 7"]
    
    static let bubbleFilenames = [BubbleType.Red:            redBubbleFilename,
                                  BubbleType.Orange:         orangeBubbleFilename,
                                  BubbleType.Green:          greenBubbleFilename,
                                  BubbleType.Blue:           blueBubbleFilename,
                                  BubbleType.Indestructible: indestructibleBubbleFilename,
                                  BubbleType.Lightning:      lightningBubbleFilename,
                                  BubbleType.Bomb:           bombBubbleFilename,
                                  BubbleType.Star:           starBubbleFilename]
    
    static let backgroundMusic       = Sound(filename: "background-music-1", type: "mp3").avAudioPlayer
    static let backgroundMusic2      = Sound(filename: "background-music-2", type: "mp3").avAudioPlayer
    static let bubbleBurstSound      = Sound(filename: "bubble-pop", type: "mp3").avAudioPlayer
    static let bubbleDropSound       = Sound(filename: "bubble-drop", type: "mp3").avAudioPlayer
    static let bubbleStopSound       = Sound(filename: "bubble-stop", type: "mp3").avAudioPlayer
    static let bubbleLightningSound  = Sound(filename: "bubble-lightning", type: "mp3").avAudioPlayer
    static let bubbleBombSound       = Sound(filename: "bubble-bomb", type: "mp3").avAudioPlayer
    static let bubbleStarSound       = Sound(filename: "bubble-star", type: "mp3").avAudioPlayer
    
    static let beginningOfMusic: NSTimeInterval = 0
    static let backgroundMusicVolume: Float     = 0.5
    static let backgroundMusicNumberOfLoops     = -1
    
    static let opaque: CGFloat      = 1.0
    static let translucent: CGFloat = 0.5
    static let transparent: CGFloat = 0.0
    
    static let bubbleRadius: CGFloat       = 32
    static let bubbleDiameter: CGFloat     = 64
    static let bubbleHeightScaled: CGFloat = bubbleDiameter * sqrt(3.0)/2
    
    static let numberOfRows         = 9
    static let numberOfColumns      = 12
    static let playModeNumberOfRows = 15
    
    static let minimumNumberOfBubblesToBurst = 3
    
    static let showBorder: CGFloat      = 1
    static let doNotShowBorder: CGFloat = 0
    
    static let subviewFront: CGFloat = 1
    
    static let repeatOnce = 1
    
    static let halfCannonWidth: CGFloat = 50
    static let cannonWidth: CGFloat     = 100
    static let cannonHeight: CGFloat    = 200
    static let cannonFrame = CGRectMake(halfScreenWidth - halfCannonWidth, totalScreenHeight - bubbleRadius - 100,
        cannonWidth, cannonHeight)
    static let baseFrame = CGRectMake(halfScreenWidth - halfCannonWidth, totalScreenHeight - bubbleDiameter,
        cannonWidth, bubbleDiameter)
    static let anchorPoint = CGPointMake(0.5, 1)
    static let vineFrame = CGRectMake(0, totalScreenHeight - bubbleDiameter - cannonHeight - 50, totalScreenWidth, 200)
    
    static let bubblesAreaHeight: CGFloat = 507.405
    static let totalScreenWidth: CGFloat  = 768
    static let halfScreenWidth: CGFloat   = 384
    static let totalScreenHeight: CGFloat = 1024
    
    static let launchingPoint           = CGPointMake(halfScreenWidth, totalScreenHeight - bubbleRadius)
    static let launchingBubbleFrame     = CGRectMake(halfScreenWidth - bubbleRadius, totalScreenHeight - bubbleDiameter,
        bubbleDiameter, bubbleDiameter)
    static let nextLaunchingBubbleFrame = CGRectMake(halfScreenWidth - 2 * bubbleDiameter, totalScreenHeight - bubbleDiameter,
        bubbleDiameter, bubbleDiameter)
    
    static let launchSpeedScaled: CGFloat = 20
    
    static let animationDuration  = 0.5
    static let noAnimationDelay   = 0.0
    static let nextAnimationDelay = 0.6
    
    static let multiplayerTimeout: NSTimeInterval = 30
    
    static let numberOfItemsShownInCollectionView: CGFloat = 12
    
    static let levelSelectorBackButtonFrame = CGRectMake(0, 0, 100, 80)
    static let levelSelectorTitleLabelFrame = CGRectMake(halfScreenWidth - 250, 0, 500, 80)
    static let levelSelectorHeaderViewFrame = CGRectMake(0, 0, totalScreenWidth, 80)
}