//
//  BubblesArea.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 31/1/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

class BubblesArea {
    typealias P = Pair<Int, Int>      // Coordinates Pair
    typealias D = [P: GameBubble?]    // Dictionary with key: Coordinates and value: Bubble's Color
    
    private var coordinatesBubbles: D
    
    /// Initialize bubbles area with 9 rows and 12 columns of empty bubbles
    init() {
        coordinatesBubbles = D()
        for row in 0..<Constants.numberOfRows {
            for column in 0..<Constants.numberOfColumns {
                let coordinates = P(row, column)
                if isValidCoordinates(coordinates) {
                    coordinatesBubbles[coordinates] = nil
                }
            }
        }
    }
    
    /// Obtain the representation of BubblesArea and return it as a dictionary (read-only)
    func toDictionary() -> D {
        return coordinatesBubbles
    }
    
    /// Return a bubble on a given coordinates if it exist, else return nil (read-only)
    func getBubbleType(coordinates: P) -> Constants.BubbleType? {
        if hasCoordinatesThatContainsBubble(coordinates) {
            let bubble = coordinatesBubbles[coordinates]!!
            return bubble.getBubbleType()
        } else {
            return nil
        }
    }
    
    /// Set a given bubble on a given coordinates
    func setBubble(coordinates: P, type: Constants.BubbleType) {
        if isValidCoordinates(coordinates) {
            let bubble: GameBubble
            switch type {
            case Constants.BubbleType.Red,
                 Constants.BubbleType.Orange,
                 Constants.BubbleType.Green,
                 Constants.BubbleType.Blue:
                bubble = BasicGameBubble(bubbleType: type)
            case Constants.BubbleType.Indestructible,
                 Constants.BubbleType.Lightning,
                 Constants.BubbleType.Bomb,
                 Constants.BubbleType.Star:
                bubble = SpecialGameBubble(bubbleType: type)
            }
            coordinatesBubbles[coordinates] = bubble
        }
    }
    
    /// Remove a bubble on a given coordinates
    func removeBubble(coordinates: P) {
        if hasCoordinates(coordinates) {
            coordinatesBubbles[coordinates]! = nil
        }
    }
    
    /// This checks if the winning condition is fulfilled
    var winningConditionFulfilled: Bool {
        /// Wins when there are no bubbles at the top row
        let row = 0
        for column in 0..<Constants.numberOfColumns {
            let coordinates = Pair(row, column)
            if coordinatesBubbles[coordinates] != nil {
                return false
            }
        }
        return true
    }
    
    /// Helper function to determine whether the coordinates given in initializer is valid.
    private func isValidCoordinates(coordinates: P) -> Bool {
        let row = coordinates.first, column = coordinates.second
        if isEven(row) {
            return row >= 0 && row < Constants.numberOfRows && column >= 0 && column < Constants.numberOfColumns
        } else {
            return row >= 0 && row < Constants.numberOfRows && column >= 0 && column < Constants.numberOfColumns - 1
        }
    }
    
    /// Helper function to check if the given coordinates exist in the bubbles area.
    private func hasCoordinates(coordinates: P) -> Bool {
        return coordinatesBubbles[coordinates] != nil
    }
    
    /// Helper function to check if the given coordinates contains a game bubble.
    private func hasCoordinatesThatContainsBubble(coordinates: P) -> Bool {
        if hasCoordinates(coordinates) {
            let bubbleOptional = coordinatesBubbles[coordinates]!
            return bubbleOptional != nil
        } else {
            return false
        }
    }
    
    /// Helper function to check if a row number is even.
    private func isEven(row: Int) -> Bool {
        return row % 2 == 0
    }
}
