//
//  LevelSelectorViewController.swift
//  BubbleBlastSaga
//
//  Created by Melvin Tan Jun Keong on 21/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import UIKit

class LevelSelectorViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var allFilenames: [[String]]!
    private let filesArchive = FilesArchive()
    var loadedBubblesArea: BubblesArea!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: flowLayout)
        collectionView!.registerClass(LevelCell.self, forCellWithReuseIdentifier: "levelCell")
        collectionView!.delegate = self
        collectionView!.dataSource = self
        collectionView!.backgroundColor = UIColor.blackColor()
        self.view.addSubview(collectionView!)
        
        let allPresetFilenames = Constants.allSoloPlayerLevelFilenames
        let allDesignedFilenames = filesArchive.getFileNames()
        allFilenames = [allPresetFilenames, allDesignedFilenames]
        
        let backButton: UIButton = UIButton(frame: Constants.levelSelectorBackButtonFrame)
        backButton.backgroundColor = UIColor.blackColor()
        backButton.setTitle("< Back", forState: UIControlState.Normal)
        backButton.setTitleColor(self.view.tintColor, forState: UIControlState.Normal)
        backButton.addTarget(self, action: "goBack", forControlEvents: UIControlEvents.TouchUpInside)
        
        let title = UILabel(frame: Constants.levelSelectorTitleLabelFrame)
        title.text = "Choose a Level to Play"
        title.textColor = UIColor.whiteColor()
        title.textAlignment = NSTextAlignment.Center
        
        let header = UIView(frame: Constants.levelSelectorHeaderViewFrame)
        header.backgroundColor = UIColor.blackColor()
        header.addSubview(title)
        header.addSubview(backButton)
        self.view.addSubview(header)
    }
    
    /// There are this number of sections
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return allFilenames.count
    }
    
    /// Make this number of cell
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allFilenames[section].count
    }
    
    /// Make cell and return override cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) ->
        UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("levelCell", forIndexPath: indexPath) as! LevelCell
        cell.textLabel.text = allFilenames[indexPath.section][indexPath.row]
        cell.textLabel.textColor = UIColor.whiteColor()
        cell.backgroundColor = UIColor.darkGrayColor()
        return cell
    }
    
    /// When the user tapped a cell, it loads the file with the corresponding file name.
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! LevelCell
        let fileName = cell.textLabel.text!
        
        if indexPath.section == 0 {
            let pathname = NSBundle.mainBundle().pathForResource(fileName, ofType: "plist")
            loadedBubblesArea = filesArchive.loadFromPropertyList(pathname!)
        } else {
            loadedBubblesArea = filesArchive.loadFromPropertyList(fileName)
        }
        if loadedBubblesArea != nil {
            self.performSegueWithIdentifier("startGameLevel", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "startGameLevel" {
            if loadedBubblesArea.winningConditionFulfilled {
                let failureAlert = UIAlertController(title: "Failed to start",
                    message: "Invalid level: Please check Level Designer for this level.",
                    preferredStyle: UIAlertControllerStyle.Alert)
                failureAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(failureAlert, animated: true, completion: nil)
            } else {
                let gamePlayViewController = segue.destinationViewController as! GamePlayViewController
                gamePlayViewController.bubblesArea = loadedBubblesArea
            }
        }
    }
    
    /// Each cell is of this size
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.frame.width, self.view.frame.height/Constants.numberOfItemsShownInCollectionView)
    }
    
    /// Padding
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let paddingFromTop = self.view.frame.height/Constants.numberOfItemsShownInCollectionView
        let paddingFromLeft: CGFloat = 0
        let paddingFromBottom: CGFloat = 0
        let paddingFromRight: CGFloat = 0
        return UIEdgeInsets(top: paddingFromTop, left: paddingFromLeft, bottom: paddingFromBottom, right: paddingFromRight)
    }
    
    func goBack() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

class LevelCell: UICollectionViewCell {
    private var textLabel: UILabel!
    private var filesArchive = FilesArchive()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let labelHeight = frame.size.height/3
        textLabel = UILabel(frame: CGRect(x: 0, y: labelHeight, width: frame.size.width, height: labelHeight))
        textLabel.textAlignment = .Center
        contentView.addSubview(textLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}