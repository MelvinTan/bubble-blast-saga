//
//  BubblesAreaView.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 3/2/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

import UIKit

class BubblesAreaView: UIView {
    typealias P = Pair<Int, Int>
    typealias D = [P: BubbleView]
    
    private var coordinatesBubbles: D
    private var playMode: Bool
    private var lastAddedBubbleCoordinates: P?
    private var lastAddedBubbleType: Constants.BubbleType?
    private var removedBubbleTypes: [Constants.BubbleType]
    
    /// Initialize bubbles area view with bubble views
    init(frame: CGRect, playMode: Bool) {
        coordinatesBubbles = D()
        self.playMode = playMode
        self.lastAddedBubbleCoordinates = nil
        self.lastAddedBubbleType = nil
        self.removedBubbleTypes = []
        super.init(frame: frame)
        let numberOfRows = playMode ? Constants.playModeNumberOfRows : Constants.numberOfRows
        for row in 0..<numberOfRows {
            for column in 0..<Constants.numberOfColumns {
                let coordinates = P(row, column)
                if isValid(coordinates) {
                    let row = coordinates.first
                    let column = coordinates.second
                    let scale = Constants.bubbleHeightScaled
                    let offsetX = isEven(row) ? Constants.bubbleDiameter * CGFloat(column) :
                                                Constants.bubbleDiameter * CGFloat(column) + Constants.bubbleRadius
                    let offsetY = CGFloat(row) * scale

                    let bubble = BubbleView(frame: CGRectMake(offsetX, offsetY, Constants.bubbleDiameter,
                        Constants.bubbleDiameter), playMode: playMode)
                    self.addSubview(bubble)
                    coordinatesBubbles[coordinates] = bubble
                }
            }
        }
    }
    
    /// Adds a bubble given the bubble type to the bubble cell in the view corresponding to the given coordinates.
    func addBubbleAtCoordinates(coordinates: P, type: Constants.BubbleType) {
        if coordinatesBubbles[coordinates] != nil {
            let bubble = coordinatesBubbles[coordinates]!
            bubble.addBubbleType(type)
            lastAddedBubbleCoordinates = coordinates
            lastAddedBubbleType = type
        }
    }
    
    /// Remove a bubble on a given coordinates from the view
    func removeBubbleAtCoordinates(coordinates: P) {
        if coordinatesBubbles[coordinates] != nil {
            let bubble = coordinatesBubbles[coordinates]!
            bubble.removeBubbleType()
        }
    }
    
    /// Remove all bubbles from the view.
    func resetBubblesArea() {
        for bubbleView in coordinatesBubbles.values {
            bubbleView.removeBubbleType()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Helper function to determine whether the coordinates given in initializer is valid.
    private func isValid(coordinates: P) -> Bool {
        let row = coordinates.first, column = coordinates.second
        let numberOfRows = playMode ? Constants.playModeNumberOfRows : Constants.numberOfRows
        if isEven(row) {
            return row >= 0 && row < numberOfRows && column >= 0 && column < Constants.numberOfColumns
        } else {
            return row >= 0 && row < numberOfRows && column >= 0 && column < Constants.numberOfColumns - 1
        }
    }
    
    /// Helper function to check if a row number is even.
    private func isEven(row: Int) -> Bool {
        return row % 2 == 0
    }
}

/// This extension is only relevant to the game play mode.
extension BubblesAreaView {
    
    /// Obtain all bubbles with bubble type
    var bubblesWithType: [BubbleView] {
        let allBubblesWithType = coordinatesBubbles.values.filter { $0.hasBubbleType() }
        return Array(allBubblesWithType)
    }
    
    /// Obtain all empty bubbles
    var bubblesWithoutType: [BubbleView] {
        let allBubblesWithoutType = coordinatesBubbles.values.filter { !$0.hasBubbleType() }
        return Array(allBubblesWithoutType)
    }
    
    /// Obtain the nearest empty cell for a given bubble
    func getNearestEmptyCell(bubble: BubbleView) -> BubbleView {
        let bubbleOptional = bubblesWithoutType.filter { CGRectContainsPoint($0.frame, bubble.center) }.first
        if bubbleOptional != nil {
            return bubbleOptional!
        } else if bubble.center.x < Constants.bubbleDiameter {
            /// Bubble is on the extreme left of an odd row
            return bubblesWithoutType.filter { CGRectContainsPoint($0.frame,
                CGPointMake(bubble.center.x + Constants.bubbleDiameter, bubble.center.y)) }.first!
        } else {
            /// Bubbles is on the extreme right of an odd row
            return bubblesWithoutType.filter { CGRectContainsPoint($0.frame,
                CGPointMake(bubble.center.x - Constants.bubbleDiameter, bubble.center.y)) }.first!
        }
    }
    
    /// Obtain the neighbouring bubbles coordinates for a given bubble coordinate
    func getNeighboursCoordinates(coordinates: P) -> [P] {
        let row = coordinates.first
        let column = coordinates.second
        let topLeft     = isEven(row) ? P(row - 1, column - 1) : P(row - 1, column)
        let topRight    = isEven(row) ? P(row - 1, column) : P(row - 1, column + 1)
        let left        = P(row, column - 1)
        let right       = P(row, column + 1)
        let bottomLeft  = isEven(row) ? P(row + 1, column - 1) : P(row + 1, column)
        let bottomRight = isEven(row) ? P(row + 1, column) : P(row + 1, column + 1)
        return [topLeft, topRight, left, right, bottomLeft, bottomRight].filter { isValid($0) }
    }
    
    /// Main function to remove the relevant bubbles
    func animateRemoveBubbles() {
        removeBubblesByBursting()
        removeBubblesBySpecialEffects()
        removeBubblesByDropping()
    }
    
    /// Returns all removed bubbles in the turn and reset the removedBubbles array to empty.
    func getRemovedBubbleTypes() -> [Constants.BubbleType] {
        let result = removedBubbleTypes
        removedBubbleTypes = []
        return result
    }
    
    /// Add multiple bubbles to the bubbles area by filling it up in a zig zag fashion
    /// So it will fill from left to right from the top row, then reverse in the next row, and so on.
    func addBubbles(bubbleTypes: [Constants.BubbleType]) {
        var bubbleTypesCopy = bubbleTypes
        if !bubbleTypesCopy.isEmpty {
            for row in 0..<Constants.playModeNumberOfRows {
                let iterators = isEven(row) ? ((0..<Constants.numberOfColumns).reverse()).reverse() :
                    (0..<Constants.numberOfColumns).reverse()
                for column in iterators {
                    let coordinates = P(row, column)
                    if isValid(coordinates) && !coordinatesBubbles[coordinates]!.hasBubbleType() {
                        if bubbleTypesCopy.isEmpty {
                            break
                        } else {
                            let bubbleType = bubbleTypesCopy.removeFirst()
                            addBubbleAtCoordinates(coordinates, type: bubbleType)
                        }
                    }
                }
            }
        }
    }

    func removeBubblesByBursting() {
        var setOfBubblesCoordinatesToBurst = Set<P>()
        
        func updateBubblesCoordinatesToBurst(coordinates: P, type: Constants.BubbleType) {
            if isValid(coordinates) {
                for neighbourCoordinates in getNeighboursCoordinates(coordinates) {
                    let neighbourBubble = coordinatesBubbles[neighbourCoordinates]!
                    if neighbourBubble.hasBubbleType() && neighbourBubble.getBubbleType()! == type &&
                        !setOfBubblesCoordinatesToBurst.contains(neighbourCoordinates) {
                        setOfBubblesCoordinatesToBurst.insert(neighbourCoordinates)
                        updateBubblesCoordinatesToBurst(neighbourCoordinates, type: type)
                    }
                }
            }
        }
        
        setOfBubblesCoordinatesToBurst.insert(lastAddedBubbleCoordinates!)
        updateBubblesCoordinatesToBurst(lastAddedBubbleCoordinates!, type: lastAddedBubbleType!)
        
        let bubblesCoordinatesToBurst = setOfBubblesCoordinatesToBurst.count >= Constants.minimumNumberOfBubblesToBurst ?
            Array(setOfBubblesCoordinatesToBurst) : []
        
        if !bubblesCoordinatesToBurst.isEmpty {
            for coordinates in bubblesCoordinatesToBurst {
                if coordinatesBubbles[coordinates] != nil {
                    let bubble = coordinatesBubbles[coordinates]!
                    removedBubbleTypes.append(bubble.getBubbleType()!)
                    bubble.removeBubbleTypeByBursting()
                }
            }
            Constants.bubbleBurstSound?.play()
        }
    }
    
    func removeBubblesBySpecialEffects() {
        let neighboursCoordinates = getNeighboursCoordinates(lastAddedBubbleCoordinates!)
        for coordinates in neighboursCoordinates {
            if let bubble = coordinatesBubbles[coordinates] {
                if bubble.hasBubbleType() {
                    switch bubble.getBubbleType()! {
                    case Constants.BubbleType.Lightning:
                        removeBubbleCoordinatesByLightning(coordinates)
                    case Constants.BubbleType.Bomb:
                        removeBubbleCoordinatesByBomb(coordinates)
                    case Constants.BubbleType.Star:
                        removeBubbleCoordinatesByStar(coordinates)
                    default:
                        ()
                    }
                }
            }
        }
    }
    
    func removeBubblesByDropping() {
        var setOfBubblesCoordinatesToKeep = Set<P>()
        var setOfBubblesCoordinatesToDrop = Set<P>()
        
        func updateBubblesCoordinatesToKeep(coordinates: P) {
            if isValid(coordinates) {
                for neighbourCoordinates in getNeighboursCoordinates(coordinates) {
                    let neighbourBubble = coordinatesBubbles[neighbourCoordinates]!
                    if neighbourBubble.hasBubbleType() && !setOfBubblesCoordinatesToKeep.contains(neighbourCoordinates) {
                        setOfBubblesCoordinatesToKeep.insert(neighbourCoordinates)
                        updateBubblesCoordinatesToKeep(neighbourCoordinates)
                    }
                }
            }
        }
        
        /// Determine which bubbles coordinates to drop by finding out what to keep
        let row = 0
        for column in 0..<Constants.numberOfColumns {
            let coordinates = P(row, column)
            if coordinatesBubbles[coordinates]!.hasBubbleType() && !setOfBubblesCoordinatesToKeep.contains(coordinates) {
                setOfBubblesCoordinatesToKeep.insert(coordinates)
                updateBubblesCoordinatesToKeep(coordinates)
            }
        }
        
        /// Drop all bubbles coordinates that are not in currentBubblesToKeep
        for bubbleCoordinates in coordinatesBubbles.keys {
            if !setOfBubblesCoordinatesToKeep.contains(bubbleCoordinates) &&
                coordinatesBubbles[bubbleCoordinates]!.hasBubbleType() {
                setOfBubblesCoordinatesToDrop.insert(bubbleCoordinates)
            }
        }
        
        if !setOfBubblesCoordinatesToDrop.isEmpty {
            for coordinates in setOfBubblesCoordinatesToDrop {
                if coordinatesBubbles[coordinates] != nil {
                    let bubble = coordinatesBubbles[coordinates]!
                    removedBubbleTypes.append(bubble.getBubbleType()!)
                    bubble.removeBubbleTypeByDropping()
                }
            }
            Constants.bubbleDropSound?.play()
        }
    }
    
    func removeBubbleCoordinatesByLightning(lightningBubbleCoordinates: P) {
        let lightningBubble = coordinatesBubbles[lightningBubbleCoordinates]!
        removedBubbleTypes.append(lightningBubble.getBubbleType()!)
        lightningBubble.removeBubbleTypeByLightning()
        
        /// Only consider bubbles that are in the same row, that is not an empty cell and is not the lightning bubble itself
        let sameRowCoordinates = coordinatesBubbles.keys.filter { $0.first == lightningBubbleCoordinates.first &&
            self.coordinatesBubbles[$0]!.hasBubbleType() && $0 != lightningBubbleCoordinates }
        var bombBubblesCoordinates = [P]()
        for coordinates in sameRowCoordinates {
            if let bubble = coordinatesBubbles[coordinates] {
                if bubble.hasBubbleType() {
                    switch bubble.getBubbleType()! {
                    case Constants.BubbleType.Bomb:
                        bombBubblesCoordinates.append(coordinates)
                    default:
                        removedBubbleTypes.append(bubble.getBubbleType()!)
                        bubble.removeBubbleTypeByLightning()
                    }
                }
            }
        }
        Constants.bubbleLightningSound?.play()
        removeBubblesByDropping()
        
        /// Handle chaining of special effects
        if !bombBubblesCoordinates.isEmpty {
            for coordinates in bombBubblesCoordinates {
                dispatch_after(
                    dispatch_time(DISPATCH_TIME_NOW, Int64(Constants.nextAnimationDelay * Double(NSEC_PER_SEC))),
                    dispatch_get_main_queue(), { () -> Void in
                        self.removeBubbleCoordinatesByBomb(coordinates)
                })
            }
        }
    }
    
    func removeBubbleCoordinatesByBomb(bombBubbleCoordinates: P) {
        let bombBubble = coordinatesBubbles[bombBubbleCoordinates]!
        if let bubbleType = bombBubble.getBubbleType() {
            removedBubbleTypes.append(bubbleType)
        } else {
            return
        }
        bombBubble.removeBubbleTypeByBomb()
        
        /// Only consider bubbles that are in the same row, that is not an empty cell and is not the lightning bubble itself
        let neighbourCoordinatesBubbles = getNeighboursCoordinates(bombBubbleCoordinates)
        var specialBubblesCoordinates = [P]()
        for coordinates in neighbourCoordinatesBubbles {
            if let bubble = coordinatesBubbles[coordinates] {
                if bubble.hasBubbleType() {
                    switch bubble.getBubbleType()! {
                    case Constants.BubbleType.Lightning, Constants.BubbleType.Bomb:
                        specialBubblesCoordinates.append(coordinates)
                    default:
                        removedBubbleTypes.append(bubble.getBubbleType()!)
                        bubble.removeBubbleTypeByBomb()
                    }
                }
            }
        }
        Constants.bubbleBombSound?.play()
        removeBubblesByDropping()
        
        /// Handle chaining of special effects
        if !specialBubblesCoordinates.isEmpty {
            for coordinates in specialBubblesCoordinates {
                dispatch_after(
                    dispatch_time(DISPATCH_TIME_NOW, Int64(Constants.nextAnimationDelay * Double(NSEC_PER_SEC))),
                    dispatch_get_main_queue(), { () -> Void in
                        if self.coordinatesBubbles[coordinates]!.getBubbleType() == Constants.BubbleType.Lightning {
                            self.removeBubbleCoordinatesByLightning(coordinates)
                        } else {
                            self.removeBubbleCoordinatesByBomb(coordinates)
                        }
                })
            }
        }
    }
    
    func removeBubbleCoordinatesByStar(starBubbleCoordinates: P) {
        let starBubble = coordinatesBubbles[starBubbleCoordinates]!
        removedBubbleTypes.append(starBubble.getBubbleType()!)
        starBubble.removeBubbleTypeByStar()
        
        /// Only consider bubbles that are of the same colour as the last added bubble
        let sameColourCoordinatesBubbles = coordinatesBubbles.keys.filter { self.coordinatesBubbles[$0]!.hasBubbleType() &&
            self.coordinatesBubbles[$0]!.getBubbleType()! == self.lastAddedBubbleType! }
        for coordinates in sameColourCoordinatesBubbles {
            if let bubble = coordinatesBubbles[coordinates] {
                removedBubbleTypes.append(bubble.getBubbleType()!)
                bubble.removeBubbleTypeByStar()
            }
        }
        Constants.bubbleStarSound?.play()
    }
    
    /// This checks if the winning condition is fulfilled
    var winningConditionFulfilled: Bool {
        /// Wins when there are no bubbles at the top row
        let row = 0
        for column in 0..<Constants.numberOfColumns {
            let coordinates = Pair(row, column)
            let bubble = coordinatesBubbles[coordinates]!
            if bubble.hasBubbleType() {
                return false
            }
        }
        return true
    }
    
    /// This checks if the losing condition is fulfilled
    var losingConditionFulfilled: Bool {
        /// Lose when the last row contains bubble(s)
        let row = Constants.playModeNumberOfRows - 1
        for column in 0..<Constants.numberOfColumns - 1 {
            let coordinates = Pair(row, column)
            let bubble = coordinatesBubbles[coordinates]!
            if bubble.hasBubbleType() {
                return true
            }
        }
        return false
    }
}
