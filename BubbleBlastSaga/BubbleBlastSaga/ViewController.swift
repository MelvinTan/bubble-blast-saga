//
//  ViewController.swift
//  LevelDesigner
//
//  Created by YangShun on 26/1/15.
//  Copyright (c) 2015 NUS CS3217. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PathsViewControllerDelegate {
    @IBOutlet var gameArea: UIView!
    @IBOutlet var palette: UIView!
    @IBOutlet var items: [UIButton]!
    private var bubblesArea = BubblesArea()
    private var bubblesAreaView = BubblesAreaView(frame: CGRectMake(0, 0,
        Constants.totalScreenWidth, Constants.bubblesAreaHeight), playMode: false)
    private var filesArchive = FilesArchive()
    private var currentItemSelected: UIButton? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let gameAreaWidth = gameArea.frame.size.width
        let gameAreaHeight = gameArea.frame.size.height
        
        addBackground(gameAreaWidth, viewHeight: gameAreaHeight)
        addPalette()
        addBubblesAreaView(gameAreaWidth)
        addGesturesToBubblesAreaView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Adds background image to the gameArea
    private func addBackground(viewWidth: CGFloat, viewHeight: CGFloat) {
        let backgroundImage = UIImage(named: Constants.backgroundFilename)
        let background = UIImageView(image: backgroundImage)
        background.frame = CGRectMake(0, 0, viewWidth, viewHeight)
        gameArea.addSubview(background)
    }
    
    /// Adds palette to the gameArea
    private func addPalette() {
        palette.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(Constants.translucent)
        gameArea.addSubview(palette)
    }
    
    /// Adds bubble cells area into the gameArea
    private func addBubblesAreaView(viewWidth: CGFloat) {
        gameArea.addSubview(bubblesAreaView)
    }
    
    /// Add tap, pan and long-press gestures into the bubblesAreaView
    func addGesturesToBubblesAreaView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleTappedCell:"))
        bubblesAreaView.addGestureRecognizer(tapGestureRecognizer)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: Selector("handlePannedCell:"))
        bubblesAreaView.addGestureRecognizer(panGestureRecognizer)
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: Selector("handleLongPressedCell:"))
        bubblesAreaView.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    /// Do appropriate actions when a bubble cell is tapped.
    func handleTappedCell(sender: UITapGestureRecognizer) {
        let eraser = items.last
        let point = sender.locationInView(bubblesAreaView)
        let bubbleView = bubblesAreaView.hitTest(point, withEvent: nil)
        if currentItemSelected == nil || currentItemSelected != eraser {
            addColoredBubbles(bubbleView, point: point, gesture: sender)
        } else {
            removeColoredBubbles(bubbleView, point: point)
        }
    }
    
    /// Do appropriate actions when a bubble cell is panned.
    func handlePannedCell(sender: UIPanGestureRecognizer) {
        let eraser = items.last
        if currentItemSelected != nil {
            let point = sender.locationInView(bubblesAreaView)
            let bubbleView = bubblesAreaView.hitTest(point, withEvent: nil)
            if currentItemSelected != eraser {
                addColoredBubbles(bubbleView, point: point, gesture: sender)
            } else {
                removeColoredBubbles(bubbleView, point: point)
            }
        }
    }
    
    /// Do appropriate actions when a bubble cell is long-pressed.
    func handleLongPressedCell(sender: UILongPressGestureRecognizer) {
        let point = sender.locationInView(bubblesAreaView)
        let bubbleView = bubblesAreaView.hitTest(point, withEvent: nil)
        removeColoredBubbles(bubbleView, point: point)
    }
    
    /// When a bubble cell is tapped,
    ///     if no items are selected, selected bubble cell will cycle through all the colors (and back to no color).
    ///     if a colored bubble is selected, selected bubble cell will turn into that color.
    /// When a bubble cell is panned, selected bubble cell will turn into the selected colored bubble.
    func addColoredBubbles(bubbleCell: UIView?, point: CGPoint, gesture: UIGestureRecognizer) {
        if bubbleCell != nil && bubbleCell!.isKindOfClass(BubbleView) {
            var bubbleType = bubblesArea.getBubbleType(point.toBubblesAreaCoordinates())
            if currentItemSelected == nil && gesture.isKindOfClass(UITapGestureRecognizer) {
                if bubbleType == nil {
                    bubbleType = Constants.BubbleType.Red
                } else {
                    switch bubbleType! {
                    case Constants.BubbleType.Red:
                        bubbleType = Constants.BubbleType.Orange
                    case Constants.BubbleType.Orange:
                        bubbleType = Constants.BubbleType.Green
                    case Constants.BubbleType.Green:
                        bubbleType = Constants.BubbleType.Blue
                    case Constants.BubbleType.Blue:
                        bubbleType = nil
                    default:
                        ()
                    }
                }
            } else {
                switch items.indexOf(currentItemSelected!)! {
                case Constants.BubbleType.Red.hashValue:
                    bubbleType = Constants.BubbleType.Red
                case Constants.BubbleType.Orange.hashValue:
                    bubbleType = Constants.BubbleType.Orange
                case Constants.BubbleType.Green.hashValue:
                    bubbleType = Constants.BubbleType.Green
                case Constants.BubbleType.Blue.hashValue:
                    bubbleType = Constants.BubbleType.Blue
                case Constants.BubbleType.Indestructible.hashValue:
                    bubbleType = Constants.BubbleType.Indestructible
                case Constants.BubbleType.Lightning.hashValue:
                    bubbleType = Constants.BubbleType.Lightning
                case Constants.BubbleType.Bomb.hashValue:
                    bubbleType = Constants.BubbleType.Bomb
                case Constants.BubbleType.Star.hashValue:
                    bubbleType = Constants.BubbleType.Star
                default:
                    bubbleType = nil
                }
            }
            
            let coordinates = point.toBubblesAreaCoordinates()
            if bubbleType == nil {
                bubblesArea.removeBubble(coordinates)
                bubblesAreaView.removeBubbleAtCoordinates(coordinates)
            } else {
                bubblesArea.setBubble(coordinates, type: bubbleType!)
                bubblesAreaView.addBubbleAtCoordinates(coordinates, type: bubbleType!)
            }
        }
    }
    
    /// Selected bubble cell will go back to no color.
    func removeColoredBubbles(bubbleCell: UIView?, point: CGPoint) {
        if bubbleCell != nil && bubbleCell!.isKindOfClass(BubbleView) {
            let coordinates = point.toBubblesAreaCoordinates()
            bubblesAreaView.removeBubbleAtCoordinates(coordinates)
            bubblesArea.removeBubble(coordinates)
        }
    }
    
    /// The following block of code is executed when user clicked on any 'Items' button.
    /// Set currentItemSelected to the item that is clicked (which may either be a colored bubble or eraser)
    @IBAction func itemSelected(sender: AnyObject) {
        let button = sender as! UIButton
        currentItemSelected = button
        for item in items {
            item.alpha = (item == button) ? Constants.opaque : Constants.translucent
        }
    }
    
    /// The following block of code is executed when user clicked on the 'Reset' button.
    @IBAction func resetBubblesArea(sender: AnyObject) {
        let resetAlert = UIAlertController(title: "Reset", message: "The board will be reset.",
            preferredStyle: UIAlertControllerStyle.Alert)
        resetAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.bubblesAreaView.resetBubblesArea()
            self.bubblesArea = BubblesArea()
        }))
        resetAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        presentViewController(resetAlert, animated: true, completion: nil)
    }
    
    /// The following block of code is executed when user clicked on the 'Save' button.
    @IBAction func saveBubblesArea(sender: AnyObject) {
        var name: UITextField?
        var savedSuccessfully = false
        let saveAlert = UIAlertController(title: "Save", message: "Name this level!",
            preferredStyle: UIAlertControllerStyle.Alert)
        saveAlert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            name = textField
            name?.placeholder = "Enter name here"
        }
        saveAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
            savedSuccessfully = self.filesArchive.saveToPropertyList(self.bubblesArea, name: name!.text!)
            if savedSuccessfully {
                let successAlert = UIAlertController(title: "Saved!", message: "You have successfully saved this level!",
                    preferredStyle: UIAlertControllerStyle.Alert)
                successAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(successAlert, animated: true, completion: nil)
            } else {
                let failureAlert = UIAlertController(title: "Failed", message: "Failed to save this level.",
                    preferredStyle: UIAlertControllerStyle.Alert)
                failureAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(failureAlert, animated: true, completion: nil)
            }
        }))
        saveAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        self.presentViewController(saveAlert, animated: true, completion: nil)
    }
    
    /// The following blocks of code is executed when user clicked on the 'Load' button.
    @IBAction func loadBubblesArea(sender: AnyObject) {
        self.performSegueWithIdentifier("showPathsTable", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPathsTable" {
            let pathsViewController = segue.destinationViewController as! PathsViewController
            pathsViewController.delegate = self
        } else if segue.identifier == "showGamePlay" {
            if bubblesAreaView.winningConditionFulfilled {
                let failureAlert = UIAlertController(title: "Failed to start",
                    message: "Invalid level: The top row must contain at least 1 bubble.",
                    preferredStyle: UIAlertControllerStyle.Alert)
                failureAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(failureAlert, animated: true, completion: nil)
            } else {
                let gamePlayViewController = segue.destinationViewController as! GamePlayViewController
                gamePlayViewController.bubblesArea = bubblesArea
            }
        }
    }
    
    func didFinishPathsViewController(controller: PathsViewController) {
        setBubblesArea(controller.loadedBubblesArea!)
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// Sets the bubbles area view using the given new bubbles area model.
    func setBubblesArea(newBubblesArea: BubblesArea) {
        bubblesAreaView.resetBubblesArea()
        bubblesArea = newBubblesArea
        let dictionary = newBubblesArea.toDictionary()
        for coordinates in dictionary.keys {
            if dictionary[coordinates]! != nil {
                bubblesAreaView.addBubbleAtCoordinates(coordinates, type: dictionary[coordinates]!!.getBubbleType())
            }
        }
    }
    
    @IBAction func goBack(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
