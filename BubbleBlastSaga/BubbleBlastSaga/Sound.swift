//
//  Sound.swift
//  GameEngine
//
//  Created by Melvin Tan Jun Keong on 8/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import UIKit
import AVFoundation

class Sound {
    var avAudioPlayer: AVAudioPlayer?
    
    init(filename: String, type: String) {
        let url = NSBundle.mainBundle().URLForResource(filename, withExtension: type)
        if url != nil {
            do {
                avAudioPlayer = try AVAudioPlayer(contentsOfURL: url!)
                avAudioPlayer!.prepareToPlay()
            } catch {
                avAudioPlayer = nil
            }
        }
    }
}