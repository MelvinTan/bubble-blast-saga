//
//  SpecialGameBubble.swift
//  BubbleBlastSaga
//
//  Created by melvint on 16/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

class SpecialGameBubble: GameBubble {
    private let possibleBubbleTypes = [Constants.BubbleType.Indestructible, Constants.BubbleType.Lightning,
        Constants.BubbleType.Bomb, Constants.BubbleType.Star]
    
    override init(bubbleType: Constants.BubbleType) {
        super.init(bubbleType: bubbleType)
        _checkRep()
    }
    
    private func _checkRep() {
        assert(isSpecial(), "The bubble is not a special bubble!")
    }
    
    private func isSpecial() -> Bool {
        return possibleBubbleTypes.contains(bubbleType)
    }
}
