//
//  MenuViewController.swift
//  BubbleBlastSaga
//
//  Created by Melvin Tan Jun Keong on 27/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// When user clicked on "MULTIPLAYER", they will be asked to start or join a group. 
    /// This affects the UI in the MultiplayerViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMultiplayer" {
            let multiplayerViewController = segue.destinationViewController as! MultiplayerViewController
            
            let settingAlert = UIAlertController(title: "Start or Join?", message: "Would you like to start or join a group?",
                preferredStyle: UIAlertControllerStyle.Alert)
            settingAlert.addAction(UIAlertAction(title: "Start", style: .Default, handler: { (action: UIAlertAction!) in
                multiplayerViewController.isLeader = true
                self.presentViewController(multiplayerViewController, animated: true, completion: nil)
            }))
            settingAlert.addAction(UIAlertAction(title: "Join", style: .Default, handler: { (action: UIAlertAction!) in
                multiplayerViewController.isLeader = false
                self.presentViewController(multiplayerViewController, animated: true, completion: nil)
            }))
            presentViewController(settingAlert, animated: true, completion: nil)
        }
    }
}