//
//  FilesArchive.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 31/1/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

import Foundation

class FilesArchive {
    
    init() {}
    
    private var documentDirectory: String {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,
            NSSearchPathDomainMask.UserDomainMask, true)
        return paths.first!
    }
    
    /// Returns: All the files' name in the document directory.
    func getFileNames() -> [String] {
        var filesInDirectory = [NSString]()
        do {
            filesInDirectory = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(documentDirectory)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        let propertyListFiles = filesInDirectory.filter { ($0 as NSString).pathExtension == "plist" }
        let propertyListFileNames = propertyListFiles.map { ($0 as NSString).stringByDeletingPathExtension }
        return propertyListFileNames
    }
    
    /// Save BubblesArea into a .plist file with given name.
    /// Returns: true if save is successful, false otherwise.
    func saveToPropertyList(bubblesArea: BubblesArea, name: String) -> Bool {
        if name.isEmpty {
            return false
        } else {
            let bubblesAreaDictionary = bubblesArea.toDictionary()
            
            let filePath = (documentDirectory as AnyObject).stringByAppendingPathComponent(name + ".plist")
            
            let dictionary = NSMutableDictionary()
            for coordinatesPair in bubblesAreaDictionary.keys {
                let bubbleOptional = bubblesAreaDictionary[coordinatesPair]!
                var value: Int {
                    if bubbleOptional == nil {
                        return -1
                    } else {
                        return bubbleOptional!.getBubbleType().hashValue
                    }
                }
                dictionary.setObject(value, forKey: "\([coordinatesPair.first, coordinatesPair.second])")
            }
            
            let isSaved = dictionary.writeToFile(filePath, atomically: true)
            return isSaved
        }
    }
    
    /// Reconstruct BubblesArea for a given filePath
    /// Returns: BubblesArea if filePath exist, nil otherwise
    func loadFromPropertyList(fileName: String) -> BubblesArea? {
        var filePath: String
        let plistExtension = ".plist"
        if fileName.containsString(plistExtension) {
            filePath = fileName
        } else {
            filePath = (documentDirectory as AnyObject).stringByAppendingPathComponent(fileName + ".plist")
        }
        var bubblesArea: BubblesArea? = nil
        if let dictionary = NSDictionary(contentsOfFile: filePath) as? [String: AnyObject] {
            bubblesArea = BubblesArea()
            for arrayString in dictionary.keys {
                let coordinatesArrayString = String(arrayString.characters.filter { $0 != "[" && $0 != "]" })
                let coordinatesArray = coordinatesArrayString.componentsSeparatedByString(", ")
                let coordinatesPair = Pair(Int(coordinatesArray[0])!, Int(coordinatesArray[1])!)
                var bubbleType: Constants.BubbleType? = nil
                let typeValue = dictionary[arrayString]! as! Int
                switch typeValue {
                case Constants.BubbleType.Red.hashValue:
                    bubbleType = Constants.BubbleType.Red
                case Constants.BubbleType.Orange.hashValue:
                    bubbleType = Constants.BubbleType.Orange
                case Constants.BubbleType.Green.hashValue:
                    bubbleType = Constants.BubbleType.Green
                case Constants.BubbleType.Blue.hashValue:
                    bubbleType = Constants.BubbleType.Blue
                case Constants.BubbleType.Indestructible.hashValue:
                    bubbleType = Constants.BubbleType.Indestructible
                case Constants.BubbleType.Lightning.hashValue:
                    bubbleType = Constants.BubbleType.Lightning
                case Constants.BubbleType.Bomb.hashValue:
                    bubbleType = Constants.BubbleType.Bomb
                case Constants.BubbleType.Star.hashValue:
                    bubbleType = Constants.BubbleType.Star
                default:
                    bubbleType = nil
                }
                if bubbleType == nil {
                    bubblesArea!.removeBubble(coordinatesPair)
                } else {
                    bubblesArea!.setBubble(coordinatesPair, type: bubbleType!)
                }
            }
        }
        return bubblesArea
    }
    
    /// Remove the plist file from the directory
    func removePropertyList(fileName: String) {
        let filePath = (documentDirectory as AnyObject).stringByAppendingPathComponent(fileName + ".plist")
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    /// Rename the original plist filename from the directory with the new plist filename
    func renamePropertyList(originalFileName: String, newFileName: String) -> Bool {
        if newFileName.isEmpty {
            return false
        } else {
            var success: Bool?
            let originalFilePath = (documentDirectory as AnyObject).stringByAppendingPathComponent(originalFileName + ".plist")
            let newFilePath = (documentDirectory as AnyObject).stringByAppendingPathComponent(newFileName + ".plist")
            do {
                try NSFileManager.defaultManager().moveItemAtPath(originalFilePath, toPath: newFilePath)
                success = true
            } catch let error as NSError {
                print(error.localizedDescription)
                success = false
            }
            return success!
        }
    }
}