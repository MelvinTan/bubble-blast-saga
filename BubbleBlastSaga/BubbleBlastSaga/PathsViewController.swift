//
//  PathsViewController.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 4/2/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

import UIKit

class PathsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,
    UICollectionViewDelegate {
    @IBOutlet var header: UIView!
    private let filesArchive = FilesArchive()
    var delegate: PathsViewControllerDelegate! = nil
    var loadedBubblesArea: BubblesArea! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: flowLayout)
        collectionView.registerClass(CustomCell.self, forCellWithReuseIdentifier: "customCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.blackColor()
        self.view.addSubview(collectionView)
        
        self.view.addSubview(header)
    }
    
    /// Make this number of cell
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filesArchive.getFileNames().count
    }
    
    /// Make cell and return cell
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) ->
        UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("customCell", forIndexPath: indexPath) as! CustomCell
        cell.pathsViewController = self
        cell.textLabel.text = filesArchive.getFileNames()[indexPath.item]
        cell.textLabel.textColor = UIColor.whiteColor()
        cell.backgroundColor = UIColor.darkGrayColor()
        return cell
    }
    
    /// When the user tapped a cell, it loads the file with the corresponding file name.
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CustomCell
        let fileName = cell.textLabel.text!
        loadedBubblesArea = filesArchive.loadFromPropertyList(fileName)
        if loadedBubblesArea != nil {
            delegate.didFinishPathsViewController(self)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// Each cell is of this size
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.frame.width, self.view.frame.height/Constants.numberOfItemsShownInCollectionView)
    }
    
    /// Padding
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let paddingFromTop = self.view.frame.height/Constants.numberOfItemsShownInCollectionView
        let paddingFromLeft: CGFloat = 0
        let paddingFromBottom: CGFloat = 0
        let paddingFromRight: CGFloat = 0
        return UIEdgeInsets(top: paddingFromTop, left: paddingFromLeft, bottom: paddingFromBottom, right: paddingFromRight)
    }
    
    /// When user tapped on the 'Back' button, the modal will be dismissed.
    @IBAction func goBack(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

protocol PathsViewControllerDelegate {
    func didFinishPathsViewController(controller: PathsViewController)
}

class CustomCell: UICollectionViewCell {
    private var textLabel: UILabel!
    private var filesArchive = FilesArchive()
    private var pathsViewController: PathsViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let labelHeight = frame.size.height/3
        textLabel = UILabel(frame: CGRect(x: 0, y: labelHeight, width: frame.size.width, height: labelHeight))
        textLabel.textAlignment = .Center
        contentView.addSubview(textLabel)
        
        addGesturesToContentView()
    }
    
    func addGesturesToContentView() {
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: Selector("handleLongPressedCell:"))
        contentView.addGestureRecognizer(longPressGestureRecognizer)
        let swipeRightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipedCell:"))
        contentView.addGestureRecognizer(swipeRightGestureRecognizer)
        let swipeLeftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipedCell:"))
        swipeLeftGestureRecognizer.direction = .Left
        contentView.addGestureRecognizer(swipeLeftGestureRecognizer)
        contentView.userInteractionEnabled = true
    }
    
    /// When the user long-pressed a cell, the user can choose to delete the file with the corresponding file name.
    func handleLongPressedCell(sender: UILongPressGestureRecognizer) {
        let tappedContentView = sender.view as UIView!
        let label = tappedContentView.subviews.first! as! UILabel
        let fileName = label.text!
        renameFile(fileName)
    }
    
    /// When the user swiped a cell, the user can choose to delete the file with the corresponding file name.
    func handleSwipedCell(sender: UISwipeGestureRecognizer) {
        let tappedContentView = sender.view as UIView!
        let label = tappedContentView.subviews.first! as! UILabel
        let fileName = label.text!
        deleteFile(fileName)
    }
    
    /// Delete a file from FilesArchive given its fileName.
    func deleteFile(fileName: String) {
        let deleteAlert = UIAlertController(title: "Delete",
            message: "'\(fileName)' will be deleted. This action cannot be undone.",
            preferredStyle: UIAlertControllerStyle.Alert)
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction!) in
            self.filesArchive.removePropertyList(fileName)
            self.pathsViewController.viewDidLoad()
        }))
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        pathsViewController.presentViewController(deleteAlert, animated: true, completion: nil)
    }
    
    func renameFile(originalFileName: String) {
        var newName: UITextField?
        let renameAlert = UIAlertController(title: "Rename", message: "Rename '\(originalFileName)' as?",
            preferredStyle: UIAlertControllerStyle.Alert)
        renameAlert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            newName = textField
            newName?.placeholder = "Enter new name here"
        }
        renameAlert.addAction(UIAlertAction(title: "Confirm", style: .Default, handler: { (action: UIAlertAction!) in
            let renamedSuccessfully = self.filesArchive.renamePropertyList(originalFileName, newFileName: newName!.text!)
            if renamedSuccessfully {
                let successAlert = UIAlertController(title: "Renamed!", message: "You have successfully renamed this level!",
                    preferredStyle: UIAlertControllerStyle.Alert)
                successAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.pathsViewController.presentViewController(successAlert, animated: true, completion: nil)
                self.pathsViewController.viewDidLoad()
            } else {
                let failureAlert = UIAlertController(title: "Failed", message: "Failed to save this level.",
                    preferredStyle: UIAlertControllerStyle.Alert)
                failureAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.pathsViewController.presentViewController(failureAlert, animated: true, completion: nil)
            }
        }))
        renameAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        pathsViewController.presentViewController(renameAlert, animated: true, completion: nil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}