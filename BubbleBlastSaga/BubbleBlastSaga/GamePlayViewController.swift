//
//  GamePlayViewController.swift
//  GameEngine
//
//  Created by Melvin Tan Jun Keong on 7/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import Foundation
import UIKit

class GamePlayViewController: UIViewController, GameEngineDelegate {
    var bubblesArea: BubblesArea!
    private var bubblesAreaView = BubblesAreaView(frame: CGRectMake(0, 0, Constants.totalScreenWidth,
        Constants.bubblesAreaHeight), playMode: true)
    @IBOutlet var gameArea: UIView!
    @IBOutlet var launchingArea: UIView!
    private var gameEngine: GameEngine!
    private var nextLaunchingBubble: BubbleView!
    private var cannon: Cannon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Initialize game by adding background and tap gesture to view, and 
        /// initialize bubbles area, cannon, game engine, and background music
        addBackground()
        addGestures()
        initializeGamePlayBubblesArea()
        initializeCannonArea()
        initializeGameEngine()
        displayNextLaunchingBubble()
        initializeBackgroundMusic()
    }
    
    func addBackground() {
        let backgroundImage = UIImage(named: Constants.backgroundFilename)
        let background = UIImageView(image: backgroundImage)
        background.frame = CGRectMake(0, 0, Constants.totalScreenWidth, Constants.totalScreenHeight)
        gameArea.addSubview(background)
        gameArea.addSubview(launchingArea)
    }
    
    func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        gameArea.addGestureRecognizer(tapGesture)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: "handlePan:")
        gameArea.addGestureRecognizer(panGesture)
    }
    
    func initializeGamePlayBubblesArea() {
        gameArea.addSubview(bubblesAreaView)
        let dictionary = bubblesArea.toDictionary()
        for coordinates in dictionary.keys {
            if dictionary[coordinates]! != nil {
                bubblesAreaView.addBubbleAtCoordinates(coordinates, type: dictionary[coordinates]!!.getBubbleType())
            }
        }
        bubblesAreaView.removeBubblesByDropping()
    }
    
    func initializeCannonArea() {
        cannon = Cannon()
        gameArea.addSubview(cannon.getCannonView())
        gameArea.addSubview(cannon.getBaseView())
        let vineImage = UIImage(named: Constants.vineFilename)
        let vine = UIImageView(image: vineImage)
        vine.frame = Constants.vineFrame
        gameArea.addSubview(vine)
    }
    
    func getNewLaunchingBubble(frame: CGRect) -> BubbleView {
        let bubbleTypesLeft = getBubbleTypesLeft()
        let randomBubbleType: Constants.BubbleType!
        if bubbleTypesLeft.count > 0 {
            let randomNumber = arc4random_uniform(UInt32(bubbleTypesLeft.count))
            randomBubbleType = bubbleTypesLeft[bubbleTypesLeft.startIndex.advancedBy(Int(randomNumber))]
        } else {
            randomBubbleType = Constants.BubbleType.Red
        }
        let launchingBubble = BubbleView(frame: frame, playMode: true)
        launchingBubble.addBubbleType(randomBubbleType)
        gameArea.userInteractionEnabled = true
        return launchingBubble
    }
    
    func getBubbleTypesLeft() -> [Constants.BubbleType] {
        var bubbleTypes = Set<Constants.BubbleType>()
        for bubble in bubblesAreaView.bubblesWithType {
            switch bubble.getBubbleType()! {
            case Constants.BubbleType.Red, Constants.BubbleType.Orange, Constants.BubbleType.Green, Constants.BubbleType.Blue:
                bubbleTypes.insert(bubble.getBubbleType()!)
            default:
                ()
            }
        }
        return Array(bubbleTypes)
    }
    
    func initializeGameEngine() {
        let launchingBubble = getNewLaunchingBubble(Constants.launchingBubbleFrame)
        gameEngine = GameEngine(gameArea: gameArea, bubble: launchingBubble, otherBubbles: bubblesAreaView.bubblesWithType)
        gameEngine.delegate = self
    }
    
    func displayNextLaunchingBubble() {
        nextLaunchingBubble = getNewLaunchingBubble(Constants.nextLaunchingBubbleFrame)
        gameArea.addSubview(nextLaunchingBubble)
    }
    
    func removeNextLaunchingBubble() {
        nextLaunchingBubble.removeFromSuperview()
        nextLaunchingBubble = nil
    }
    
    func initializeBackgroundMusic() {
        Constants.backgroundMusic?.currentTime = Constants.beginningOfMusic
        Constants.backgroundMusic?.volume = Constants.backgroundMusicVolume
        Constants.backgroundMusic?.numberOfLoops = Constants.backgroundMusicNumberOfLoops
        Constants.backgroundMusic?.play()
    }
    
    func handlePan(sender: UIPanGestureRecognizer) {
        let point = sender.locationInView(gameArea)
        /// Rotate cannon only if user taps in the allowable region (which is above the launching bubble)
        if point.y < Constants.totalScreenHeight - Constants.bubbleDiameter {
            cannon.rotateImageToPoint(point)
        }
    }
    
    /// Bubble will be launched when user tap the screen, in the direction of the tapped point.
    func handleTap(sender: UITapGestureRecognizer) {
        gameArea.userInteractionEnabled = false
        cannon.getCannonView().startAnimating()
        gameEngine.launchBubble(cannon.getDirection())
    }
    
    /// gameEngine will execute these action when bubble has stopped moving.
    func updateLaunchedBubbleStatus(engine: GameEngine) {
        let launchedBubble = engine.getBubble() as! BubbleView
        let nearestEmptyCell = bubblesAreaView.getNearestEmptyCell(launchedBubble)
        let coordinates = nearestEmptyCell.center.toBubblesAreaCoordinates()
        bubblesAreaView.addBubbleAtCoordinates(coordinates, type: launchedBubble.getBubbleType()!)
        bubblesAreaView.animateRemoveBubbles()
        engine.removeBubbleFromView()
        
        checkCondition(engine)
    }
    
    func checkCondition(engine: GameEngine) {
        if UIApplication.sharedApplication().isIgnoringInteractionEvents() {
            dispatch_after(
                dispatch_time(DISPATCH_TIME_NOW, Int64(Constants.nextAnimationDelay * Double(NSEC_PER_SEC))),
                dispatch_get_main_queue(), { () -> Void in
                    self.checkCondition(engine)
            })
        } else {
            if bubblesAreaView.winningConditionFulfilled {
                removeNextLaunchingBubble()
                doWinningAction()
            } else if bubblesAreaView.losingConditionFulfilled {
                removeNextLaunchingBubble()
                doGameOverAction()
            } else {
                let newLaunchingBubble = nextLaunchingBubble
                newLaunchingBubble.frame = Constants.launchingBubbleFrame
                displayNextLaunchingBubble()
                engine.setBubble(newLaunchingBubble)
                engine.setOtherBubbles(bubblesAreaView.bubblesWithType)
            }
        }
    }
    
    /// Handle Win Game
    func doWinningAction() {
        Constants.backgroundMusic?.stop()
        gamePlayAlert("Congratulations!", message: "You have successfully cleared this level!")
    }
    
    /// Handle Game Over
    func doGameOverAction() {
        Constants.backgroundMusic?.stop()
        gamePlayAlert("Game Over!", message: "You lost. Better luck next time!")
    }
    
    /// When game ends, allow user to choose whether to play again or go back to level designer.
    func gamePlayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Play Again", style: .Default, handler: { (action: UIAlertAction!) in
            self.doPlayAgainAction()
        }))
        alert.addAction(UIAlertAction(title: "Go Back", style: .Default, handler: { (action: UIAlertAction!) in
            self.removeCannonFromView()
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    /// Handle Reset
    func doPlayAgainAction() {
        bubblesAreaView.resetBubblesArea()
        initializeGamePlayBubblesArea()
        initializeGameEngine()
        displayNextLaunchingBubble()
        initializeBackgroundMusic()
    }
    
    /// User will be brought back to the previous page when "Back" button is tapped.
    @IBAction func goBack(sender: AnyObject) {
        Constants.backgroundMusic?.stop()
        removeCannonFromView()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// This removes the cannon imageView from the view controller.
    func removeCannonFromView() {
        cannon.getCannonView().removeFromSuperview()
        cannon.getBaseView().removeFromSuperview()
        cannon = nil
    }
}
