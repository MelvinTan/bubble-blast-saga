//
//  GameEngine.swift
//  GameEngine
//
//  Created by Melvin Tan Jun Keong on 10/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import UIKit
import AVFoundation

protocol GameEngineDelegate {
    /// This is for user to define the actions after the bubble animation terminates.
    func updateLaunchedBubbleStatus(engine: GameEngine)
}

class GameEngine {
    private var gameArea: UIView
    private var bubble: UIView
    private var otherBubbles: [UIView]
    var delegate: GameEngineDelegate! = nil
    private var bubbleCollideSoundPlayer: AVAudioPlayer? = nil
    private let totalScreenWidth: CGFloat = 768
    private let totalScreenHeight: CGFloat = 1024
    private let frameDuration = 0.01
    
    init(gameArea: UIView, bubble: UIView, otherBubbles: [UIView]) {
        self.gameArea = gameArea
        self.bubble = bubble
        self.otherBubbles = otherBubbles
        gameArea.addSubview(bubble)
        
        bubbleCollideSoundPlayer = loadSound("bubble-stop", type: "mp3")
    }
    
    func getBubble() -> UIView {
        return bubble
    }
    
    func setBubble(newBubble: UIView) {
        bubble.removeFromSuperview()
        bubble = newBubble
        gameArea.addSubview(bubble)
    }
    
    func removeBubbleFromView() {
        bubble.removeFromSuperview()
    }
    
    func setOtherBubbles(newOtherBubbles: [UIView]) {
        otherBubbles = newOtherBubbles
    }
    
    /// This launches the bubble from the current position in the direction of the tapped point.
    /// Bubble will travel in a straight line at a constant speed.
    /// Bubble will rebound when it hit the edges of the screen.
    /// Bubble will stop when it fulfills a given set of conditions (when it collides the top wall or another bubble).
    func launchBubble(vector: CGPoint) {
        var direction = vector
        if bubbleTouchesLeft() || bubbleTouchesRight() {
            direction = reflectX(direction)
            self.bubbleCollideSoundPlayer?.play()
        }
        if bubbleTouchesTop() || bubbleTouchesBottom() {
            direction = reflectY(direction)
            self.bubbleCollideSoundPlayer?.play()
        }
        
        UIView.animateWithDuration(frameDuration,
            delay: 0,
            options: .CurveLinear,
            animations: {
                self.bubble.center.x += direction.x
                self.bubble.center.y += direction.y
            },
            completion: { finished in
                if self.bubbleTouchesTop() || self.bubbleTouchesOtherBubbles() {
                    self.delegate.updateLaunchedBubbleStatus(self)
                    self.bubbleCollideSoundPlayer?.play()
                    return
                } else {
                    self.launchBubble(direction)
                }
            }
        )
    }
    
    private var bubbleRadius: CGFloat {
        return bubble.frame.width/2
    }
    
    private func reflectX(direction: CGPoint) -> CGPoint {
        return CGPointMake(-direction.x, direction.y)
    }
    
    private func reflectY(direction: CGPoint) -> CGPoint {
        return CGPointMake(direction.x, -direction.y)
    }
    
    private func bubbleTouchesLeft() -> Bool {
        return bubble.center.x - bubbleRadius < 0
    }
    
    private func bubbleTouchesRight() -> Bool {
        return bubble.center.x + bubbleRadius > totalScreenWidth
    }
    
    private func bubbleTouchesTop() -> Bool {
        return bubble.center.y - bubbleRadius < 0
    }
    
    private func bubbleTouchesBottom() -> Bool {
        return bubble.center.y + bubbleRadius > totalScreenHeight
    }
    
    private func distanceBetween(point1: CGPoint, point2: CGPoint) -> CGFloat {
        return CGFloat(hypotf(Float(point1.x - point2.x), Float(point1.y - point2.y)))
    }
    
    private func bubbleTouchesOtherBubbles() -> Bool {
        /// Two bubbles are said to collide if the distance between their center is less than two times radius.
        let bubblesCollided = otherBubbles.filter { distanceBetween($0.center, point2: bubble.center) <= bubbleRadius * 2 }
        return !bubblesCollided.isEmpty
    }
    
    private func loadSound(filename: String, type: String) -> AVAudioPlayer? {
        var avAudioPlayer: AVAudioPlayer? = nil
        let url = NSBundle.mainBundle().URLForResource(filename, withExtension: type)
        if url != nil {
            do {
                avAudioPlayer = try AVAudioPlayer(contentsOfURL: url!)
                avAudioPlayer!.prepareToPlay()
            } catch {
                avAudioPlayer = nil
            }
        }
        return avAudioPlayer
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}