//
//  GameBubble.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 5/2/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

class GameBubble {
    internal let bubbleType: Constants.BubbleType
    
    init(bubbleType: Constants.BubbleType) {
        self.bubbleType = bubbleType
    }
    
    func getBubbleType() -> Constants.BubbleType {
        return bubbleType
    }
}