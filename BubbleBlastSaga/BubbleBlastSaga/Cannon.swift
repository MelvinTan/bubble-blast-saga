//
//  Cannon.swift
//  BubbleBlastSaga
//
//  Created by Melvin Tan Jun Keong on 22/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import UIKit

class Cannon {
    private var cannonView: UIImageView
    private var baseView: UIImageView
    private var direction: CGPoint
    
    init() {
        func convertCIImageToCGImage(inputImage: CIImage) -> CGImage! {
            let context = CIContext(options: nil)
            return context.createCGImage(inputImage, fromRect: inputImage.extent)
        }
        
        var imageListArray = [UIImage]()
        let imageName = Constants.cannonFramesFilename
        let image  = UIImage(named: imageName)
        let imageWidth = image!.size.width
        let imageHeight = image!.size.height
        let numberOfRows = 2, numberOfColumns = 6
        let cannonImageWidth = imageWidth/CGFloat(numberOfColumns)
        let cannonImageHeight = imageHeight/CGFloat(numberOfRows)
        for row in 0..<numberOfRows {
            for column in 0..<numberOfColumns {
                let cropRect = CGRectMake(CGFloat(cannonImageWidth*CGFloat(column)),
                    CGFloat(cannonImageHeight*CGFloat(row)), cannonImageWidth, cannonImageHeight)
                let imageRef = CGImageCreateWithImageInRect(convertCIImageToCGImage(CIImage(image: image!)!), cropRect)
                let croppedImage = UIImage(CGImage: imageRef!)
                imageListArray.append(croppedImage)
            }
        }
        cannonView = UIImageView(frame: Constants.cannonFrame)
        cannonView.contentMode = .ScaleAspectFill
        cannonView.animationImages = imageListArray
        cannonView.animationDuration = Constants.animationDuration
        cannonView.animationRepeatCount = Constants.repeatOnce
        cannonView.image = imageListArray.first!
        cannonView.layer.zPosition = Constants.subviewFront
        cannonView.layer.anchorPoint = Constants.anchorPoint
        
        let cannonBaseImage = UIImage(named: Constants.baseFilename)
        baseView = UIImageView(image: cannonBaseImage)
        baseView.frame = Constants.baseFrame
        baseView.layer.zPosition = Constants.subviewFront
        baseView.layer.anchorPoint = Constants.anchorPoint
        
        let upwards = CGPointMake(Constants.halfScreenWidth, 0)
        direction = Constants.launchingPoint.standardizedDirectionTo(upwards).scale(Constants.launchSpeedScaled)
    }
    
    func getCannonView() -> UIImageView {
        return cannonView
    }
    
    func getBaseView() -> UIImageView {
        return baseView
    }
    
    func getDirection() -> CGPoint {
        return direction
    }
    
    /// For a given panned point, the cannon and base will turn to face in that direction.
    func rotateImageToPoint(panPoint: CGPoint) {
        direction = Constants.launchingPoint.standardizedDirectionTo(panPoint).scale(Constants.launchSpeedScaled)
        
        let offsetX = panPoint.x - Constants.launchingPoint.x
        let offsetY = panPoint.y - Constants.launchingPoint.y
        let angle = atan(offsetX/offsetY)
        cannonView.transform = CGAffineTransformMakeRotation(-angle)
        baseView.transform = CGAffineTransformMakeRotation(-angle)
    }
}