//
//  Pair.swift
//  GraphADT
//
//  Created by Melvin Tan Jun Keong on 19/1/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//


struct Pair<T1: Equatable, T2: Equatable> {
    var first: T1
    var second: T2
    
    init(_ first: T1, _ second: T2) {
        self.first = first
        self.second = second
    }
}

extension Pair : Hashable {
    var hashValue: Int {
        return "\(first)".hashValue ^ "\(second)".hashValue
    }
}

// MARK: Equatable

func ==<T1, T2>(lhs: Pair<T1, T2>, rhs: Pair<T1, T2>) -> Bool {
    return lhs.first == rhs.first && lhs.second == rhs.second
}