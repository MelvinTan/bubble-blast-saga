//
//  MultiplayerViewController.swift
//  BubbleBlastSaga
//
//  Created by Melvin Tan Jun Keong on 22/2/16.
//  Copyright © 2016 Melvin Tan Jun Keong. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

class MultiplayerViewController: UIViewController, GameEngineDelegate, MCBrowserViewControllerDelegate,
    MCSessionDelegate {
    var isLeader: Bool!
    private var bubblesAreaView = BubblesAreaView(frame: CGRectMake(0, 0, Constants.totalScreenWidth,
        Constants.bubblesAreaHeight), playMode: true)
    private var background: UIImageView!
    @IBOutlet var gameArea: UIView!
    @IBOutlet var launchingArea: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var pleaseWaitLabel: UILabel!
    private var filesArchive = FilesArchive()
    private var gameEngine: GameEngine!
    private var nextLaunchingBubble: BubbleView!
    private var cannon: Cannon!
    private var receivedBubblesMessage = ""
    let allPresetFilenames = Constants.allMultiPlayerLevelFilenames
    @IBOutlet var browseButton: UIButton!
    private var timer: NSTimer!
    private var survivorPeersDisplayNames: Set<String>!
    
    let serviceType = "TransferBubbles"
    var browser : MCBrowserViewController!
    var assistant : MCAdvertiserAssistant!
    var session : MCSession!
    var peerID: MCPeerID!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Initialize view controller by adding Multipeer Connectivity logic, and background image
        /// The rest are initialized upon start of the game.
        
        peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
        session = MCSession(peer: peerID)
        session.delegate = self
        
        browser = MCBrowserViewController(serviceType: serviceType, session: session)
        browser.delegate = self;
        
        assistant = MCAdvertiserAssistant(serviceType: serviceType, discoveryInfo: nil, session: session)
        if !isLeader {
            assistant.start()
            browseButton.hidden = true
        } else {
            pleaseWaitLabel.hidden = true
        }

        addBackground()
    }
    
    func addBackground() {
        let backgroundImage = UIImage(named: Constants.backgroundFilename)
        background = UIImageView(image: backgroundImage)
        background.frame = CGRectMake(0, 0, Constants.totalScreenWidth, Constants.totalScreenHeight)
        gameArea.addSubview(background)
        gameArea.addSubview(launchingArea)
    }
    
    func changeBackground() {
        let newBackgroundImage = UIImage(named: Constants.background2Filename)
        background.image = newBackgroundImage
    }
    
    /// The following are initialized upon game start (i.e. once the leader click "DONE").
    func initializeGame(randomFilename: String) {
        changeBackground()                            /// Change background color to a more competitive one
        initializeGamePlayBubblesArea(randomFilename) /// Initialize bubbles area with one random multiplayer preset level
        addGestures()
        initializeCannonArea()
        initializeGameEngine()
        displayNextLaunchingBubble()
        initializePeersLoseState()                    /// Initialize an array of peers who have not lost yet.
        initializeBackgroundMusic()
        initializeTimer()                             /// Initialize a timeout to prevent peers from cheating.
        
        /// The following buttons are kept hidden upon game start so no one is allowed to do any weird things.
        browseButton.hidden = true
        backButton.hidden = true
        pleaseWaitLabel.hidden = true
        
        assistant.stop()
    }
    
    func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        gameArea.addGestureRecognizer(tapGesture)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: "handlePan:")
        gameArea.addGestureRecognizer(panGesture)
    }
    
    func initializeGamePlayBubblesArea(filename: String) {
        gameArea.addSubview(bubblesAreaView)
        let pathname = NSBundle.mainBundle().pathForResource(filename, ofType: "plist")
        let bubblesArea = filesArchive.loadFromPropertyList(pathname!)
        
        let dictionary = bubblesArea!.toDictionary()
        for coordinates in dictionary.keys {
            if dictionary[coordinates]! != nil {
                bubblesAreaView.addBubbleAtCoordinates(coordinates, type: dictionary[coordinates]!!.getBubbleType())
            }
        }
        bubblesAreaView.removeBubblesByDropping()
    }
    
    func initializeCannonArea() {
        cannon = Cannon()
        gameArea.addSubview(cannon.getCannonView())
        gameArea.addSubview(cannon.getBaseView())
        let vineImage = UIImage(named: Constants.vineFilename)
        let vine = UIImageView(image: vineImage)
        vine.frame = Constants.vineFrame
        gameArea.addSubview(vine)
    }
    
    func getNewLaunchingBubble(frame: CGRect) -> BubbleView {
        let bubbleTypesLeft = getBubbleTypesLeft()
        let randomBubbleType: Constants.BubbleType!
        if bubbleTypesLeft.count > 0 {
            let randomNumber = arc4random_uniform(UInt32(bubbleTypesLeft.count))
            randomBubbleType = bubbleTypesLeft[bubbleTypesLeft.startIndex.advancedBy(Int(randomNumber))]
        } else {
            randomBubbleType = Constants.BubbleType.Red
        }
        let launchingBubble = BubbleView(frame: frame, playMode: true)
        launchingBubble.addBubbleType(randomBubbleType)
        gameArea.userInteractionEnabled = true
        return launchingBubble
    }
    
    func getBubbleTypesLeft() -> [Constants.BubbleType] {
        var bubbleTypes = Set<Constants.BubbleType>()
        for bubble in bubblesAreaView.bubblesWithType {
            switch bubble.getBubbleType()! {
            case Constants.BubbleType.Red, Constants.BubbleType.Orange, Constants.BubbleType.Green, Constants.BubbleType.Blue:
                bubbleTypes.insert(bubble.getBubbleType()!)
            default:
                ()
            }
        }
        return Array(bubbleTypes)
    }
    
    func initializeGameEngine() {
        let launchingBubble = getNewLaunchingBubble(Constants.launchingBubbleFrame)
        gameEngine = GameEngine(gameArea: gameArea, bubble: launchingBubble, otherBubbles: bubblesAreaView.bubblesWithType)
        gameEngine.delegate = self
    }
    
    func displayNextLaunchingBubble() {
        nextLaunchingBubble = getNewLaunchingBubble(Constants.nextLaunchingBubbleFrame)
        gameArea.addSubview(nextLaunchingBubble)
    }
    
    func removeNextLaunchingBubble() {
        nextLaunchingBubble.removeFromSuperview()
        nextLaunchingBubble = nil
    }
    
    func initializePeersLoseState() {
        survivorPeersDisplayNames = []
        for eachPeerID in session.connectedPeers {
            let peerDisplayName = "\(eachPeerID.displayName)"
            survivorPeersDisplayNames.insert(peerDisplayName)
        }
    }
    
    func initializeBackgroundMusic() {
        Constants.backgroundMusic2?.currentTime = Constants.beginningOfMusic
        Constants.backgroundMusic2?.volume = Constants.backgroundMusicVolume
        Constants.backgroundMusic2?.numberOfLoops = Constants.backgroundMusicNumberOfLoops
        Constants.backgroundMusic2?.play()
    }
    
    func initializeTimer() {
        timer = NSTimer.scheduledTimerWithTimeInterval(Constants.multiplayerTimeout, target: self,
            selector: "doGameOverAction", userInfo: nil, repeats: true)
    }
    
    func resetTimer() {
        timer?.invalidate()
        timer = NSTimer.scheduledTimerWithTimeInterval(Constants.multiplayerTimeout, target: self,
            selector: "doGameOverAction", userInfo: nil, repeats: true)
    }
    
    func handlePan(sender: UIPanGestureRecognizer) {
        let point = sender.locationInView(gameArea)
        /// Rotate cannon only if user taps in the allowable region (which is above the launching bubble)
        if point.y < Constants.totalScreenHeight - Constants.bubbleDiameter {
            cannon.rotateImageToPoint(point)
        }
    }
    
    /// Bubble will be launched when user tap the screen, in the direction of the tapped point.
    func handleTap(sender: UITapGestureRecognizer) {
        gameArea.userInteractionEnabled = false
        cannon.getCannonView().startAnimating()
        gameEngine.launchBubble(cannon.getDirection())
        resetTimer()
    }
    
    /// gameEngine will execute these action when bubble has stopped moving.
    func updateLaunchedBubbleStatus(engine: GameEngine) {
        let launchedBubble = engine.getBubble() as! BubbleView
        let nearestEmptyCell = bubblesAreaView.getNearestEmptyCell(launchedBubble)
        let coordinates = nearestEmptyCell.center.toBubblesAreaCoordinates()
        bubblesAreaView.addBubbleAtCoordinates(coordinates, type: launchedBubble.getBubbleType()!)
        bubblesAreaView.animateRemoveBubbles()
        engine.removeBubbleFromView()
        
        checkCondition(engine)
    }
    
    func checkCondition(engine: GameEngine) {
        if UIApplication.sharedApplication().isIgnoringInteractionEvents() {
            dispatch_after(
                dispatch_time(DISPATCH_TIME_NOW, Int64(Constants.nextAnimationDelay * Double(NSEC_PER_SEC))),
                dispatch_get_main_queue(), { () -> Void in
                    self.checkCondition(engine)
            })
        } else {
            sendBubbles(bubblesAreaView.getRemovedBubbleTypes())
            dispatch_async(dispatch_get_main_queue()) {
                self.addReceivedBubblesToBubblesArea()
                if self.bubblesAreaView.winningConditionFulfilled {
                    self.removeNextLaunchingBubble()
                    self.doWinningAction()
                } else if self.bubblesAreaView.losingConditionFulfilled {
                    self.removeNextLaunchingBubble()
                    self.doGameOverAction()
                } else {
                    let newLaunchingBubble = self.nextLaunchingBubble
                    newLaunchingBubble.frame = Constants.launchingBubbleFrame
                    self.displayNextLaunchingBubble()
                    engine.setBubble(newLaunchingBubble)
                    engine.setOtherBubbles(self.bubblesAreaView.bubblesWithType)
                }
            }
        }
    }
    
    /// Sends the removed bubbles to all peers.
    func sendBubbles(bubbleTypes: [Constants.BubbleType]) {
        var message = ""
        for bubbleType in bubbleTypes {
            message += "\(bubbleType.hashValue)"
        }
        if !message.isEmpty {
            sendMessage(message)
        }
    }
    
    /// This is a wrapper to send any message to all peers, given a message string.
    func sendMessage(message: String) {
        let data = (message as NSString).dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        do {
            try session.sendData(data!, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable)
        } catch let error as NSError {
            print("Error sending data: \(error.localizedDescription)")
        }
    }
    
    func addReceivedBubblesToBubblesArea() {
        var bubbleTypes: [Constants.BubbleType] = []
        for character in receivedBubblesMessage.characters {
            let string = String(character)
            if let hashValue = Int(string) {
                switch hashValue {
                case Constants.BubbleType.Red.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Red)
                case Constants.BubbleType.Orange.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Orange)
                case Constants.BubbleType.Green.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Green)
                case Constants.BubbleType.Blue.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Blue)
                case Constants.BubbleType.Indestructible.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Indestructible)
                case Constants.BubbleType.Lightning.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Lightning)
                case Constants.BubbleType.Bomb.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Bomb)
                case Constants.BubbleType.Star.hashValue:
                    bubbleTypes.append(Constants.BubbleType.Star)
                default:
                    ()
                }
            }
        }
        bubblesAreaView.addBubbles(bubbleTypes)
        receivedBubblesMessage = ""
    }
    
    /// Handle Win Game
    func doWinningAction() {
        sendMessage("Won: \(peerID.displayName)")
        gamePlayAlert("Congratulations!", message: "You won!")
    }
    
    /// Handle the case when everyone else lost.
    func doEveryoneElseLostAction() {
        gamePlayAlert("Congratulations!", message: "You are the only survivor left. You won!")
    }
    
    /// Handle Game Over
    func doGameOverAction() {
        sendMessage("Lost: \(peerID.displayName)")
        gamePlayAlert("Game Over!", message: "You lost. Better luck next time!")
    }
    
    /// Handle the case when someone else won.
    func doSomeoneElseWonAction(winnerDisplayName: String) {
        gamePlayAlert("Game Over!", message: "You lost. The winner is \(winnerDisplayName)!")
    }
    
    /// When game ends, allow user to choose whether to play again or go back to level designer.
    func gamePlayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
            Constants.backgroundMusic2?.stop()
            self.dismissViewControllerAnimated(true, completion: nil)
            self.removeCannonFromView()
            self.assistant.stop()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    /// This removes the cannon imageView from the ViewController
    func removeCannonFromView() {
        cannon.getCannonView().removeFromSuperview()
        cannon.getBaseView().removeFromSuperview()
        cannon = nil
    }
    
    @IBAction func goBack(sender: AnyObject) {
        self.assistant.stop()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// This allows the browser to start browsing for followers to connect.
    @IBAction func showBrowser(sender: AnyObject) {
        presentViewController(browser, animated: true, completion: nil)
    }
    
    /// This happens when the leader taps on "DONE" and the game starts.
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController)  {
        self.dismissViewControllerAnimated(true, completion: nil)
        let randomNumber = arc4random_uniform(UInt32(allPresetFilenames.count))
        sendMessage("Random: \(randomNumber)")
        let randomFilename = allPresetFilenames[allPresetFilenames.startIndex.advancedBy(Int(randomNumber))]
        initializeGame(randomFilename)
        print("\(session.connectedPeers.first!.displayName)")
    }
    
    /// This happens when the leader taps on "CANCEL"
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController)  {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// Messages are received and logic are handled here.
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID)  {
        // Called when a peer sends an NSData to us
        dispatch_async(dispatch_get_main_queue()) {
            let message = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
            let messageArray = message.componentsSeparatedByString(": ")
            if !messageArray.isEmpty && messageArray.first! == "Random" {
                let randomNumber = messageArray[1]
                let randomFilename = self.allPresetFilenames[self.allPresetFilenames.startIndex.advancedBy(Int(randomNumber)!)]
                self.initializeGame(randomFilename)
            } else if !messageArray.isEmpty && messageArray.first! == "Won" {
                let winnerDisplayName = messageArray[1]
                self.doSomeoneElseWonAction(winnerDisplayName)
            } else if !messageArray.isEmpty && messageArray.first! == "Lost" {
                let lostPeerDisplayName = messageArray[1]
                self.survivorPeersDisplayNames.remove(lostPeerDisplayName)
                if self.survivorPeersDisplayNames.isEmpty {
                    self.doEveryoneElseLostAction()
                }
            } else {
                self.receivedBubblesMessage += message
            }
        }
    }
    
    /// Useless functions to conform to MCSessionDelegate protocol
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String,
        fromPeer peerID: MCPeerID, withProgress progress: NSProgress)  {
    }
    
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String,
        fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?)  {
    }
    
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String,
        fromPeer peerID: MCPeerID)  {
    }
    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState)  {
    }
}
