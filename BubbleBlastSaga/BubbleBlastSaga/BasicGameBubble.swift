//
//  BasicGameBubble.swift
//  LevelDesigner
//
//  Created by Melvin Tan Jun Keong on 5/2/16.
//  Copyright © 2016 NUS CS3217. All rights reserved.
//

class BasicGameBubble: GameBubble {
    private let possibleBubbleTypes = [Constants.BubbleType.Red, Constants.BubbleType.Orange,
        Constants.BubbleType.Green, Constants.BubbleType.Blue]
    
    override init(bubbleType: Constants.BubbleType) {
        super.init(bubbleType: bubbleType)
        _checkRep()
    }
    
    private func _checkRep() {
        assert(isAColor(), "The bubble is not a basic coloured bubble!")
    }
    
    private func isAColor() -> Bool {
        return possibleBubbleTypes.contains(bubbleType)
    }
}
